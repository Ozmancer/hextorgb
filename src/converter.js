const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // may return single char
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // ff0000
    },

    hexToRGB: (hex) => {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        redRBG = parseInt(result[1], 16),
        blueRBG = parseInt(result[2], 16),
        greenRBG = parseInt(result[3], 16)
        return (`${redRBG}, ${blueRBG}, ${greenRBG}`)     
    }
}